const requestBaseUrl = `${contextRoot}rest/posts`

let csrfToken = null

function setCsrfToken(token) {
  csrfToken = token
}

function fetchPost(postId) {
  return fetch(`${requestBaseUrl}/${postId}`).then((response) =>
    response.json()
  )
}

function fetchPosts(page = 0, repliesToId = null, sortBy = 'timestamp') {
  if (repliesToId) {
    return fetch(
      `${requestBaseUrl}?page=${page}&repliesToId=${repliesToId}&sortBy=${sortBy}`
    ).then((response) => response.json())
  }
  return fetch(`${requestBaseUrl}?page=${page}&sortBy=${sortBy}`)
    .then((response) => response.json())
}

function fetchPostsOfUser(username, page = 0) {
  return fetch(`${requestBaseUrl}?posterUsername=${username}&page=${page}`)
    .then((response) => response.json())
}

function createPost(content, responseToId = null) {
  
  const headers = {
    'Content-Type': 'application/json',
    'X-CSRF-TOKEN': csrfToken,
  }
  const body = { content }
  const requestUrl = responseToId
    ? `${requestBaseUrl}/${responseToId}`
    : requestBaseUrl

  return fetch(requestUrl, {
    method: 'POST',
    headers,
    body: JSON.stringify(body),
  }).then((response) => {fetchPost(responseToId); return response.json()})
}

function updatePost(content, postId) {
  const headers = {
    'Content-Type': 'application/json',
    'X-CSRF-TOKEN': csrfToken,
  }
  const body = { content }

  return fetch(`${requestBaseUrl}/${postId}`, {
    method: 'PUT',
    headers,
    body: JSON.stringify(body),
  }).then((response) => response.json())
}

function deletePost(postId) {
  const headers = { 'X-CSRF-TOKEN': csrfToken }
  return fetch(`${requestBaseUrl}/${postId}`, { method: 'DELETE', headers })
}

function addReaction(postId, reactionType) {
  const headers = {
    'Content-Type': 'application/json',
    'X-CSRF-TOKEN': csrfToken
  }
  const body = { reactionType }

  return fetch(`${requestBaseUrl}/${postId}/reactions`, {
    method: 'POST',
    headers,
    body: JSON.stringify(body)
  }).then((response) => response.json())
}

function likePost(postId) {
  return addReaction(postId, 'LIKE')
}

export default {
  setCsrfToken,
  fetchPost,
  fetchPosts,
  createPost,
  updatePost,
  deletePost,
  likePost,
  addReaction,
  fetchPostsOfUser
}
