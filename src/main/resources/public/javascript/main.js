let inputUsername = false
let inputPassword = false
let inputEmail = false
let inputRegisterUsername = false
let inputRegisterPassword = false
let inputPasswordRepeat = false
let repeatPasswordValue = ''

const registerFormFields = [
  { id: 'username-register', label: 'username-label-register' },
  { id: 'email', label: 'email-label' }
]

const hasErrors = document.querySelector('.register-form-error') !== null

if (hasErrors) {
  registerFormFields.forEach(({ id, label }) => {
    moveLabel(id, label)
    checkField(id)
  })

  const sigupForm = document.getElementById('signup-form')
  const loginForm = document.getElementById('login-form')
  sigupForm.setAttribute('class', 'signup-form signup-form-show')
  loginForm.setAttribute('class', 'login-form login-form-hide')
}

function moveLabel(id, label) {
  const labelName = document.getElementById(label)
  const idName = document.getElementById(id).value

  if (idName.length) {
    labelName.removeAttribute('class', 'form-input-label')
    labelName.setAttribute('class', 'label-shrink')
  } else {
    labelName.removeAttribute('class', 'label-shrink')
    labelName.setAttribute('class', 'form-input-label')
  }
}

//form label move to the border animation
function labelTop(id, label) {
  moveLabel(id, label)
  checkField(id)
}

function checkField(id) {
  const value = document.getElementById(id).value
  const letters = /^[A-Za-z0-9]+$/

  if (id === 'username') {
    const username = document.getElementById('usernameNoti')
    const usernameStar = document.getElementById('requireUsername')
    const checkUsername = document.getElementById('checkUsername')

    if (!value.match(letters) || value.length < 3 || value.length > 14) {
      username.className = 'show-notification color-red center'
      checkUsername.className = 'fas fa-check hide-notification'
      inputUsername = false
    } else {
      username.className = 'hide-notification color-red center'
      checkUsername.className = 'fas fa-check show-notification'
      usernameStar.style.display = 'none'
      inputUsername = true
    }
  } else if (id === 'password') {
    const password = document.getElementById('passwordNoti')
    const passwordStar = document.getElementById('requirePassword')
    const checkPassword = document.getElementById('checkPassword')

    if (!value.match(letters) || value.length < 8) {
      password.className = 'show-notification color-red center'
      checkPassword.className = 'fas fa-check hide-notification'
      inputPassword = false
    } else {
      password.className = 'hide-notification color-red center'
      checkPassword.className = 'fas fa-check show-notification'
      passwordStar.style.display = 'none'
      inputPassword = true
    }

  } else if (id === 'email') {
    const email = document.getElementById('emailNoti')
    const emailStar = document.getElementById('requireEmail')
    const checkEmail = document.getElementById('checkEmail')

    const emailIsValid = /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(value)

    if (!emailIsValid) {
      email.className = 'show-notification color-red center'
      checkEmail.className = 'fas fa-check hide-notification'
      inputEmail = false
    } else {
      email.className = 'hide-notification color-red center'
      checkEmail.className = 'fas fa-check show-notification'
      emailStar.style.display = 'none'
      inputEmail = true
    }

  } else if (id === 'username-register') {
    const userRegister = document.getElementById('usernameRegisterNoti')
    const userRegisterStar = document.getElementById('requireUsernameRegister')
    const checkUsernameRegister = document.getElementById('checkUsernameRegister')

    if (!value.match(letters) || value.length < 3 || value.length > 14) {
      userRegister.className = 'show-notification color-red center'
      checkUsernameRegister.className = 'fas fa-check hide-notification'
      inputRegisterUsername = false
    } else {
      userRegister.className = 'hide-notification color-red center'
      checkUsernameRegister.className = 'fas fa-check show-notification'
      userRegisterStar.style.display = 'none'
      inputRegisterUsername = true
    }
  } else if (id === 'password-register') {
    repeatPasswordValue = value

    const userRegisterPassword = document.getElementById('passwordRegisterNoti')
    const userRegisterPasswordStar = document.getElementById('requireRegisterPassword')
    const checkRegisterPassword = document.getElementById('checkRegisterPassword')

    if (!value.match(letters) || value.length < 8) {
      userRegisterPassword.className = 'show-notification color-red center'
      checkRegisterPassword.className = 'fas fa-check hide-notification'
      inputRegisterPassword = false
    } else {
      userRegisterPassword.className = 'hide-notification color-red center'
      checkRegisterPassword.className = 'fas fa-check show-notification'
      userRegisterPasswordStar.style.display = 'none'
      inputRegisterPassword = true
    }
  } else if (id === 'password-repeat') {
    const userPasswordRepeat = document.getElementById('passwordRepeatNoti')
    const userPasswordRepeatStar = document.getElementById('userPasswordRepeatStar')
    const checkRepeatPassword = document.getElementById('checkRepeatPassword')

    if (!value.match(letters) || value.length < 8 || value !== repeatPasswordValue) {
      userPasswordRepeat.className = 'show-notification color-red center'
      checkRepeatPassword.className = 'fas fa-check hide-notification'
      inputPasswordRepeat = false
    } else {
      userPasswordRepeat.className = 'hide-notification color-red center'
      checkRepeatPassword.className = 'fas fa-check show-notification'
      userPasswordRepeatStar.style.display = 'none'
      inputPasswordRepeat = true
    }
  }

  if (inputPassword && inputUsername) {
    const loginbtn = document.getElementById('login-button')
    loginbtn.className= 'signin-button'
    loginbtn.removeAttribute('disabled')
  }

  if (inputEmail && inputRegisterUsername && inputRegisterPassword && inputPasswordRepeat) {
    const registerbtn = document.getElementById('register-button')
    registerbtn.className= 'register-button'
    registerbtn.removeAttribute('disabled')
  }

}

const registerBtn = document.getElementById('signup-btn')
const loginBtn = document.getElementById('login-btn')

//animation when user click on the "register" button, display signup form
registerBtn.addEventListener('click', () => {
  const sigupForm = document.getElementById('signup-form')
  const loginForm = document.getElementById('login-form')
  sigupForm.setAttribute('class', 'signup-form signup-form-show')
  loginForm.setAttribute('class', 'login-form login-form-hide')
})

//animation when user click on the "login" button, display login form
loginBtn.addEventListener('click', () => {
  const sigupForm = document.getElementById('signup-form')
  const loginForm = document.getElementById('login-form')
  sigupForm.setAttribute('class', 'signup-form signup-form-hide')
  loginForm.setAttribute('class', 'login-form login-form-show')
})


