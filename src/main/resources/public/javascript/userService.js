const requestBaseUrl = `${contextRoot}rest/users`

let csrfToken = null

function setCsrfToken(token) {
  csrfToken = token
}

function updateUser(id, changes) {
  const headers = {
    'Content-Type': 'application/json',
    'X-CSRF-TOKEN': csrfToken,
  }

  return fetch(`${requestBaseUrl}/${id}`, {
    method: 'PUT',
    headers,
    body: JSON.stringify(changes),
  }).then((response) => response.json())
}

export default {
  setCsrfToken,
  updateUser
}
