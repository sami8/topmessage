package group3.topmessage.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import group3.topmessage.service.DatabaseUserDetailsService;

/**
 * Security configuration for Spring Security, used in development and testing environments.
 */
@Profile({"dev", "test"})
@Configuration
public class DevSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private DatabaseUserDetailsService userDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
            .antMatchers(HttpMethod.GET, "/signin").permitAll()
            .antMatchers(HttpMethod.POST, "/signin").permitAll()
            .antMatchers(HttpMethod.GET, "/css", "/css/**").permitAll()
            .antMatchers(HttpMethod.GET, "/javascript", "/javascript/**").permitAll()
            .antMatchers(HttpMethod.GET, "/images", "/images/**").permitAll()
            .antMatchers("/test", "/test/**").permitAll()
            .anyRequest().authenticated();
        http.formLogin()
            .loginPage("/signin")
            .defaultSuccessUrl("/")
            .failureUrl("/signin?error")
            .loginProcessingUrl("/login")
            .permitAll();
        http.logout()
            .permitAll();
        http.sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.ALWAYS);
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
            .userDetailsService(userDetailsService)
            .passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(WebSecurity sec) throws Exception {
        sec.ignoring()
            .antMatchers("/h2-console", "/h2-console/**");
    }
}
