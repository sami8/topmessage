package group3.topmessage.config;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import group3.topmessage.persistence.entity.Post;
import group3.topmessage.persistence.entity.User;
import group3.topmessage.persistence.repository.PostRepository;
import group3.topmessage.service.UserService;
import group3.topmessage.service.dto.UserRegistrationDetails;

/**
 * Inserts some test data to the database. Used in development environment.
 */
@Profile("dev")
@Component
public class TestDataLoader implements ApplicationRunner {

    @Autowired
    private UserService userService;

    @Autowired
    private PostRepository postRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        List<User> users = new ArrayList<>();

        for (int i = 0; i < 20; ++i) {
            UserRegistrationDetails u = new UserRegistrationDetails();
            u.setUsername("user"+i);
            u.setName("user"+i);
            u.setEmail("user"+i+"@somewebsite.com");
            u.setPassword("userpassword"+i);
            u.setPasswordRepeat("userpassword"+i);
            users.add(userService.register(u));
        }

        for (User u : users) {
            Post p = new Post();
            p.setContent("content "+u.getName());
            p.setPoster(u);
            p.setTimestamp(LocalDateTime.now());
            postRepository.save(p);
        }
    }
}
