package group3.topmessage.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.Instant;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

/**
 * Interceptor of incoming HTTP-requests, for logging purposes.
 */
@Component
public class RequestInterceptor extends HandlerInterceptorAdapter {

    private static final Logger logger = LoggerFactory.getLogger(RequestInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response,
                             Object handler) throws Exception {
        long startTime = Instant.now().toEpochMilli();
        request.setAttribute("startTime", startTime);

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(request.getMethod());
        stringBuilder.append(" :: ");
        stringBuilder.append(request.getRequestURI());

        Map<String, String[]> params = request.getParameterMap();
        Set<Map.Entry<String, String[]>> entries = params.entrySet();

        if (!entries.isEmpty()) {
            stringBuilder.append("\n\tRequest parameters:\n");

            for (Map.Entry<String, String[]> entry : entries) {
                String key = entry.getKey();
                String[] values = entry.getValue();

                stringBuilder.append("\t");
                stringBuilder.append(key);
                stringBuilder.append("=");
                stringBuilder.append(Arrays.toString(values));
                stringBuilder.append("\n");
            }
        }

        logger.info(stringBuilder.toString());
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response,
                                Object handler, Exception ex) throws Exception {
        long startTime = (Long) request.getAttribute("startTime");
        long timeTaken = Instant.now().toEpochMilli() - startTime;

        logger.info(
            "Time taken :: "+timeTaken+" ms, for "+
            request.getMethod()+" "+request.getRequestURI()
        );
    }
}
