package group3.topmessage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TopmessageApplication {

    public static void main(String[] args) {
        SpringApplication.run(TopmessageApplication.class, args);
    }

}
