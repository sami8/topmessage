package group3.topmessage.service;

import java.io.IOException;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import group3.topmessage.persistence.entity.File;
import group3.topmessage.persistence.entity.User;
import group3.topmessage.persistence.repository.UserRepository;
import group3.topmessage.service.dto.UserRegistrationDetails;

/**
 * Provides {@link User} related services for the controllers.
 */
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FileService fileService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * Get the currently authenticated user.
     */
    public User getAuthenticatedUser() {
        String username = SecurityContextHolder
            .getContext()
            .getAuthentication()
            .getName();

        return userRepository.findByUsername(username);
    }

    /**
     * Get the currently authenticated user's roles
     * @return the roles as a Set
     */
    public Set<String> getAuthenticatedUserRoles() {
        return SecurityContextHolder.getContext()
            .getAuthentication()
            .getAuthorities()
            .stream()
            .map(a -> a.getAuthority())
            .collect(Collectors.toSet());
    }

    /**
     * Get a user by username
     * @param username
     * @return user with the username
     */
    public User getUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    /**
     * Get user by id
     * @param id
     * @return
     */
    public Optional<User> getUserById(Long id) {
        return userRepository.findById(id);
    }

    /**
     * Creates a new {@link User} from the given details.
     * @param details
     * @return the new User that was created
     */
    public User register(UserRegistrationDetails details) {
        String rawPass    = details.getPassword();
        String hashedPass = passwordEncoder.encode(rawPass);

        User user = new User();
        user.setUsername(details.getUsername());
        user.setPassword(hashedPass);
        user.setEmail(details.getEmail());
        user.setName(details.getName());

        return userRepository.save(user);
    }

    /**
     * Updates the user with the changes, skipping any null values.
     * Also skips username and password.
     * @param user
     * @param changes
     * @return updated User
     */
    public User updateUser(User user, User changes) {
        ModelMapper m = new ModelMapper();

        m.typeMap(User.class, User.class).addMappings((mapper) -> {
            mapper.skip(User::setUsername);
            mapper.skip(User::setPassword);
        });
        m.map(changes, user);

        return userRepository.save(user);
    }

    /**
     * Creates a new {@link File} of the multipartFile and sets it as
     * the profile picture of the user.
     * @param user
     * @param multipartFile
     * @return updated User
     * @throws IOException in case there there was an access
     *   error when getting the byte array of multipartFile
     */
    public User updateUserProfilePicture(User user,
                                         MultipartFile multipartFile) throws IOException {
        File file = fileService.createFile(multipartFile);
        User changes = new User();
        changes.setProfilePicture(file);

        return updateUser(user, changes);
    }
}
