package group3.topmessage.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import group3.topmessage.persistence.entity.File;
import group3.topmessage.persistence.repository.FileRepository;

/**
 * Provides {@link File} related services for the controllers.
 */
@Service
public class FileService {

    @Autowired
    private FileRepository fileRepository;

    /**
     * Get all Files from the database.
     * @return List of Files
     */
    public List<File> getAllFiles() {
        return fileRepository.findAll();
    }

    /**
     * Get one file from the database.
     * @param id
     * @return Optional of File
     */
    public Optional<File> getOneFile(Long id) {
        return fileRepository.findById(id);
    }

    /**
     * Create a new file to the database from a MultipartFile.
     * @param multipartFile
     * @return the created File
     * @throws IOException in case there was an access error
     *   when getting the byte array of multipartFile
     */
    public File createFile(MultipartFile multipartFile)
                                                    throws IOException {
        File file = new File();
        file.setName(multipartFile.getOriginalFilename());
        file.setMediaType(multipartFile.getContentType());
        file.setSize(multipartFile.getSize());
        file.setContent(multipartFile.getBytes());

        return fileRepository.save(file);
    }
}
