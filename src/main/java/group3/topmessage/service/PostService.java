package group3.topmessage.service;

import java.time.LocalDateTime;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import group3.topmessage.persistence.entity.Post;
import group3.topmessage.persistence.entity.Reaction;
import group3.topmessage.persistence.entity.User;
import group3.topmessage.persistence.entity.Reaction.ReactionType;
import group3.topmessage.persistence.repository.PostRepository;
import group3.topmessage.persistence.repository.ReactionRepository;
import group3.topmessage.service.dto.PostPageRequest;

/**
 * Provides {@link Post} related services for the controllers.
 */
@Service
public class PostService {

    @Autowired
    private UserService userService;
    
    @Autowired
    private PostRepository postRepository;

    @Autowired
    private ReactionRepository reactionRepository;

    /**
     * Get a page of posts from the database with specified parameters.
     * 
     * See also {@link PostPageRequest}
     * @param req request parameters
     * @return a Page of Posts
     */
    public Page<Post> getPosts(PostPageRequest req) {
        Pageable pageable =
            PageRequest.of(
                req.getPage(),
                req.getSize(),
                Sort.by(req.getSortBy()).descending()
            );
        
        if (req.getPoster() != null && req.getRepliesTo() != null) {
            return postRepository.findByPosterAndRepliesTo(
                req.getPoster(), req.getRepliesTo(), pageable
            );
        }
        if (req.getPoster() != null) {
            return postRepository.findByPoster(req.getPoster(), pageable);
        }
        if (req.getRepliesTo() != null) {
            return postRepository.findByRepliesTo(req.getRepliesTo(), pageable);
        }
        return postRepository.findByRepliesToIsNull(pageable);
    }

    /**
     * Get one Post from the database.
     * @return Optional of {@link Post}, since the Post might not exist
     */
    public Optional<Post> getOnePost(Long postId) {
        return postRepository.findById(postId);
    }

    /**
     * Create a new opening post for the currently authenticated user.
     * @return Post that was saved to the database
     */
    public Post createPost(Post newPost) {
        User user = userService.getAuthenticatedUser();
        newPost.setPoster(user);
        newPost.setTimestamp(LocalDateTime.now());

        return postRepository.save(newPost);
    }

    /**
     * Create a new reply to a post for the currently authenticated user.
     * @return Post that was saved to the database
     */
    public Post createComment(Post repliesTo, Post newComment) {
        User user = userService.getAuthenticatedUser();
        newComment.setPoster(user);
        newComment.setRepliesTo(repliesTo);
        newComment.setTimestamp(LocalDateTime.now());

        return postRepository.save(newComment);
    }

    /**
     * Deletes a Post from the database.
     * @param post Post to be deleted
     */
    public void deletePost(Post post) {
        postRepository.delete(post);
    }

    /**
     * Update the Post with changes.
     * @param original Post to be updated
     * @param changes changes to the original
     * @return updated Post
     */
    public Post updatePost(Post original, Post changes) {
        original.setContent(changes.getContent());
        original.setUpdated(LocalDateTime.now());

        return postRepository.save(original);
    }

    /**
     * Creates a {@link Reaction} to a {@link Post} for the currently authenticated user.
     * If the user already reacted to the {@link Post}, the old one will be removed.
     * If the new reaction is also of same ReactionType of the old one, no new reaction
     * will be added after old one is deleted.
     * @param post Post that would be reacted to
     * @param reaction Reaction for the post
     * @return updated Post that was reacted to
     */
    @Transactional
    public Post createOrDeleteReaction(Post post, Reaction reaction) {
        User user = userService.getAuthenticatedUser();

        if (reaction.getReactionType() == ReactionType.LIKE) {
            return createOrDeleteLike(user, post);
        }
        return createOrDeleteReaction(user, post, reaction);
    }

    @Transactional
    private Post createOrDeleteLike(User user, Post post) {
        Reaction reaction = reactionRepository
            .findByReactedByAndReactedToAndReactionType(
                user, post, ReactionType.LIKE
            );

        if (reaction != null) {
            post = removeReaction(post, reaction);
        } else {
            reaction = new Reaction();
            reaction.setReactionType(ReactionType.LIKE);
            post = saveReaction(user, post, reaction);
        }
        postRepository.refresh(post);
        return post;
    }

    @Transactional
    private Post createOrDeleteReaction(User user, Post post, Reaction reaction) {
        Reaction reactionInDb = reactionRepository
            .findByReactedByAndReactedToAndReactionTypeNotLike(
                user, post, ReactionType.LIKE
            );

        if (reactionInDb != null) {
            post = removeReaction(post, reactionInDb);

            if (reactionInDb.getReactionType() != reaction.getReactionType()) {
                post = saveReaction(user, post, reaction);
            }
        } else {
            post = saveReaction(user, post, reaction);
        }
        postRepository.refresh(post);
        return post;
    }

    @Transactional
    private Post removeReaction(Post post, Reaction r) {
        reactionRepository.delete(r);
        post.getReactions().remove(r);

        return postRepository.saveAndFlush(post);
    }

    @Transactional
    private Post saveReaction(User user, Post post, Reaction reaction) {
        reaction.setReactionType(reaction.getReactionType());
        reaction.setReactedBy(user);
        reaction.setReactedTo(post);
        reactionRepository.save(reaction);

        post.getReactions().add(reaction);
        return postRepository.saveAndFlush(post);
    }
}
