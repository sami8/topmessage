package group3.topmessage.service.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import group3.topmessage.validation.constraint.NotAlreadyTaken;
import group3.topmessage.validation.constraint.PasswordsMatch;
import lombok.Data;

/**
 * Class used by {@link group3.topmessage.service.UserService} for details in the registration form.
 */
@Data
@PasswordsMatch(message = "validation.error.passwordsmatch")
public class UserRegistrationDetails {
    
    @Size(
        min = 3, max = 14,
        message = "validation.error.usernamelength"
    )
    @NotAlreadyTaken(
        field = NotAlreadyTaken.Field.USERNAME,
        message = "validation.error.usernametaken"
    )
    private String username;
    
    private String name;
    
    @Email
    @NotAlreadyTaken(
        field = NotAlreadyTaken.Field.EMAIL,
        message = "validation.error.emailtaken"
    )
    private String email;
    
    @Pattern(
        regexp = ".{8,}",
        message = "validation.error.password"
    )
    private String password;
    
    private String passwordRepeat;
    
    public void setUsername(String username) {
        this.username = username.trim();
    }
    
    public void setName(String name) {
        this.name = name.trim();
    }
    
    public void setEmail(String email) {
        this.email = email.trim();
    }
    
    public void setPassword(String password) {
        this.password = password.trim();
    }
    
    public void setPasswordRepeat(String passwordRepeat) {
        this.passwordRepeat = passwordRepeat.trim();
    }
}