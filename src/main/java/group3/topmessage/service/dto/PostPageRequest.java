package group3.topmessage.service.dto;

import group3.topmessage.persistence.entity.Post;
import group3.topmessage.persistence.entity.User;
import lombok.Data;

/**
 * Class used by {@link group3.topmessage.service.PostService} as parameters for
 * for finding Posts from the database.
 */
@Data
public class PostPageRequest {
    private User poster;
    private Post repliesTo;
    private Integer page = 0;
    private Integer size = 10;
    private String sortBy = "timestamp";
}
