package group3.topmessage.service;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import group3.topmessage.persistence.entity.User;
import group3.topmessage.persistence.repository.UserRepository;

/**
 * UserDetailsService implementation needed by Spring Security to know
 * how to find the user's details with a username that is included in the HTTP request.
 */
@Service
public class DatabaseUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User u = userRepository.findByUsername(username);

        if (u == null) {
            throw new UsernameNotFoundException("User with username \""+username+"\" does not exist.");
        }
        return new org.springframework.security.core.userdetails.User(
                u.getUsername(), u.getPassword(), true, true, true, true,
                Arrays.asList(new SimpleGrantedAuthority("USER"))
        );
    }
}
