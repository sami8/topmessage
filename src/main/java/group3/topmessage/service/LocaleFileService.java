package group3.topmessage.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Service;

/**
 * Service for finding available Vue localization resources from public/locale
 */
@Service
public class LocaleFileService {

    @Autowired
    private ResourcePatternResolver resolver;

    /**
     * Reads public/locale to find files that match "vue_messages_COUNTRY_CODE.json"
     * @return A Map with country codes as keys and filenames as values
     * @throws IOException if public/locale can't be accessed
     */
    public Map<String, String> availableVueLocaleFiles() throws IOException {
        Map<String, String> files = new HashMap<>();
        Resource[] resources = resolver.getResources("classpath:public/locale/*.json");
        Pattern p = Pattern.compile("vue_messages_(.+)\\.json$");

        for (Resource r : resources) {
            Matcher m = p.matcher(r.getFilename());

            if (m.find()) {
                String ext = m.group(1);
                files.put(ext, r.getFilename());
            }
        }
        return files;
    }
}
