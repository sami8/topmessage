package group3.topmessage.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import group3.topmessage.persistence.entity.User;
import group3.topmessage.persistence.repository.FileRepository;
import group3.topmessage.persistence.repository.PostRepository;
import group3.topmessage.persistence.repository.ReactionRepository;
import group3.topmessage.persistence.repository.UserRepository;

/**
 * Helper service used in tests.
 */
@Service
public class TestService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private ReactionRepository reactionRepository;

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Transactional
    public void resetDatabase() {
        fileRepository.deleteAll();
        reactionRepository.deleteAll();
        postRepository.deleteAll();
        userRepository.deleteAll();
    }
    
    public void addTestUser() {
        String username = "testuser";
        String password = "password";
        String hashedPassword = passwordEncoder.encode(password);

        User u = new User();

        u.setUsername(username);
        u.setPassword(hashedPassword);
        userRepository.save(u);
    }
}
