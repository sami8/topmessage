package group3.topmessage.controller.rest;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import group3.topmessage.persistence.entity.File;
import group3.topmessage.service.FileService;

/**
 * RestController for {@link File} related resources.
 */
@RestController
public class FileRestController {

    public static final String BASE_URL = "/rest/files";

    @Autowired
    private FileService fileService;

    /**
     * @return All {@link File}s from the database (content not included)
     */
    @GetMapping(BASE_URL)
    public List<File> files() {
        return fileService.getAllFiles();
    }

    /**
     * Responds the with content of the file with specified id.
     * @param id id of the {@link File}
     * @return ResponseEntity with the bytes of the File as the body
     * @throws ResponseStatusException with HttpStatus.NOT_FOUND
     *   if a File with specifiec id does not exist
     */
    @GetMapping(BASE_URL+"/{id}")
    public ResponseEntity<byte[]> getFile(@PathVariable Long id) {
        File fo = fileService.getOneFile(id)
            .orElseThrow(() -> {
                return new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    "File with id "+id+" does not exist."
                );
            });

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType(fo.getMediaType()));
        headers.setContentLength(fo.getSize());
        headers.add("Content-Disposition", "attachment; filename=" + fo.getName());

        return ResponseEntity.ok()
            .headers(headers)
            .body(fo.getContent());
    }

    /**
     * Creates a new File to the database.
     * @param multipartFile
     * @return created File that will be converted to JSON (file content not included)
     * @throws IOException in case there was an access error
     *    when getting the byte array of multipartFile
     */
    @PostMapping(BASE_URL)
    public File uploadFile(@RequestParam("file") MultipartFile multipartFile) throws IOException {
        return fileService.createFile(multipartFile);
    }
}
