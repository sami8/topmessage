package group3.topmessage.controller.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import group3.topmessage.persistence.entity.Post;
import group3.topmessage.persistence.entity.Reaction;
import group3.topmessage.persistence.entity.User;
import group3.topmessage.service.PostService;
import group3.topmessage.service.UserService;
import group3.topmessage.service.dto.PostPageRequest;

/**
 * RestController for {@link Post} related resources.
 */
@RestController
public class PostRestController {

    public static final String BASE_URL = "/rest/posts";

    @Autowired
    private UserService userService;

    @Autowired
    private PostService postService;

    /**
     * Returns a {@link Page} of {@link Post}s that will be converted to JSON.
     * @param repliesToId request parameter for finding posts that reply to a post with the id
     * @param posterUsername request parameter for finding posts that were posted by a user with the username
     * @param requestParams see {@link PostPageRequest}
     * @return a {@link Page} of Posts
     */
    @GetMapping(BASE_URL)
    public Page<Post> allPosts(@RequestParam(required = false) Long repliesToId,
                               @RequestParam(required = false) String posterUsername,
                               PostPageRequest requestParams) {
        if (posterUsername != null) {
            User poster = findUserOrThrowStatusNotFound(posterUsername);
            requestParams.setPoster(poster);
        }
        if (repliesToId != null) {
            Post repliesTo = findPostOrThrowStatusNotFound(repliesToId);
            requestParams.setRepliesTo(repliesTo);
        }
        return postService.getPosts(requestParams);
    }

    /**
     * Returns one Post with specified id from the database that
     * will be converted to JSON for the requester.
     * @return a Post with the id parameter
     * @throws ResponseStatusException with HttpStatus.NOT_FOUND
     *  if the Post does not exist.
     */
    @GetMapping(BASE_URL+"/{id}")
    public Post onePost(@PathVariable Long id) {
        return findPostOrThrowStatusNotFound(id);
    }

    /**
     * Creates a new Post to the database and returns the created Post which
     * will be converted to JSON for the client.
     * @return Post that was saved to the database
     */
    @PostMapping(BASE_URL)
    public Post createPost(@RequestBody @Valid Post requestBody) throws Exception {
        return postService.createPost(requestBody);
    }

    /**
     * Creates a new reply/comment to a Post, saves it to the database and returns the saved Post which
     * will be converted to JSON for the client.
     * @return Post that was saved to the database
     * @throws ResponseStatusException with HttpStatus.NOT_FOUND
     *  if the Post with specified id does not exist.
     */
    @PostMapping(BASE_URL+"/{id}")
    public Post createComment(@RequestBody @Valid Post requestBody,
                              @PathVariable Long id) throws Exception {
        Post repliesTo = findPostOrThrowStatusNotFound(id);
        return postService.createComment(repliesTo, requestBody);
    }

    /**
     * Creates a new Reaction to a Post and saves it into the database.
     * If the user had already reacted to the Post, the old reaction will be deleted.
     * Returns the Post which will be converted to JSON for the requester.
     * @return Post that was saved to the database
     * @throws ResponseStatusException with HttpStatus.NOT_FOUND
     *  if the Post with specified id does not exist.
     */
    @PostMapping(BASE_URL+"/{id}/reactions")
    public Post createReaction(@PathVariable Long id, @RequestBody Reaction reaction) {
        Post post = findPostOrThrowStatusNotFound(id);
        return postService.createOrDeleteReaction(post, reaction);
    }

    /**
     * Deletes the Post with specified id from the database and responds with HTTP status code NO CONTENT.
     * @throws ResponseStatusException with HttpStatus.NOT_FOUND if the Post with specified id does not exist.
     * @throws ResponseStatusException with HttpStatus.UNAUTHORIZED
     *  if the currently authenticated user is not the poster of the Post
     */
    @DeleteMapping(BASE_URL+"/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deletePost(@PathVariable Long id) {
        Post post = findPostOrThrowStatusNotFound(id);
        User authenticatedUser = userService.getAuthenticatedUser();

        if (!post.getPoster().equals(authenticatedUser)) {
            throw new ResponseStatusException(
                HttpStatus.UNAUTHORIZED, "You do not have rights to remove the post."
            );
        }
        postService.deletePost(post);
    }

    /**
     * Updates the Post with the specified id with values from the request body.
     * @return the updated Post which will be converted to JSON
     * @throws ResponseStatusException with HttpStatus.NOT_FOUND
     *  if the Post with specified id does not exist.
     * @throws ResponseStatusException with HttpStatus.UNAUTHORIZED
     *  if currently authenticated user is not the poster of the Post
     */
    @PutMapping(BASE_URL+"/{id}")
    public Post updatePost(@PathVariable Long id,
                           @RequestBody @Valid Post requestBody) {
        Post post = findPostOrThrowStatusNotFound(id);
        User authenticatedUser = userService.getAuthenticatedUser();

        if (!post.getPoster().equals(authenticatedUser)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
        return postService.updatePost(post, requestBody);
    }

    private Post findPostOrThrowStatusNotFound(Long id) {
        return postService.getOnePost(id)
            .orElseThrow(() -> {
                return new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    "Post with id " + id + " does not exist."
                );
            });
    }

    private User findUserOrThrowStatusNotFound(String username) {
        User u = userService.getUserByUsername(username);
        if (u == null) {
            throw new ResponseStatusException(
                HttpStatus.NOT_FOUND,
                "User with username " + username+ " does not exist."
            );
        }
        return u;
    }
}
