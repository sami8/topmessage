package group3.topmessage.controller.rest;

import java.io.IOException;
import java.util.Set;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import group3.topmessage.persistence.entity.User;
import group3.topmessage.service.UserService;
import group3.topmessage.validation.constraint.ProfilePictureConstraint;


/**
 * RestController for {@link User} related resources.
 */
@RestController
@Validated
public class UserRestController {

    public static final String BASE_URL = "/rest/users";

    @Autowired
    private UserService userService;

    @GetMapping(BASE_URL+"/{id}")
    public User getUser(@PathVariable Long id) {
        User userOfId = userService.getUserById(id)
            .orElseThrow(() -> {
                return new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    "User with id "+id+" does not exist."
                );
            });

        return userOfId;
    }

    @PutMapping(BASE_URL+"/{id}")
    public User updateUser(@PathVariable Long id,
                           @RequestBody @Valid User requestBody)
                           throws Exception {
        User userOfId = userService.getUserById(id)
            .orElseThrow(() -> {
                return new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    "User with id "+id+" does not exist."
                );
            });
        
        User authenticated = userService.getAuthenticatedUser();
        Set<String> roles  = userService.getAuthenticatedUserRoles();

        if (!roles.contains("admin") && !userOfId.equals(authenticated)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
        return userService.updateUser(authenticated, requestBody);
    }   

    @Transactional
    @PostMapping(BASE_URL+"/{id}/profilepic")
    @ResponseBody
    public User updateUserProfilePicture(@PathVariable Long id,
                                         @RequestParam("file")
                                         @ProfilePictureConstraint MultipartFile multipartFile)
                                         throws IOException {
        User userOfId = userService.getUserById(id)
            .orElseThrow(() -> {
                return new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    "User with id "+id+" does not exist."
                );
            });
        
        User authenticated = userService.getAuthenticatedUser();
        Set<String> roles  = userService.getAuthenticatedUserRoles();

        if (!roles.contains("admin") && !userOfId.equals(authenticated)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }

        return userService.updateUserProfilePicture(userOfId, multipartFile);
    }
    
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    private String handleConstraintViolationException(ConstraintViolationException e) {
        return "Validation error: " + e.getMessage();
    }
}
