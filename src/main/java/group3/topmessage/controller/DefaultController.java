package group3.topmessage.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Controller that handles all GET requests not mapped to any other
 * controller.
 */
@Controller
public class DefaultController {

    @GetMapping("*")
    public String home() {
        return "redirect:/posts";
    }
}

