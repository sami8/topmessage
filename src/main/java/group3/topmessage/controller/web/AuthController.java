package group3.topmessage.controller.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import group3.topmessage.service.UserService;
import group3.topmessage.service.dto.UserRegistrationDetails;

/**
 * Controller that handles requests related to signing up and signing in.
 */
@Controller
public class AuthController {
    
    @Autowired
    private UserService userService;

    /**
     * Responds with a page containing login and register forms.
     * @param details see {@link UserRegistrationDetails}
     * @return template "welcome"
     */
    @GetMapping("/signin")
    public String loginAndRegisterPage(@ModelAttribute("user")
                                       UserRegistrationDetails details) {
        return "welcome";
    }

    /**
     * Validates input from register form and creates a new user to the
     * database if it is valid.
     * @param details Object containing user input of the form, see {@link UserRegistrationDetails}
     * @param bindingResult
     * @return template "welcome" if there are validation errors or
     *  redirection to / if registration was successful
     */
    @PostMapping("/signin")
    public String register(@ModelAttribute("user")
                           @Valid UserRegistrationDetails details,
                           BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "welcome";
        }
        userService.register(details);
        return "redirect:/";
    }
}
