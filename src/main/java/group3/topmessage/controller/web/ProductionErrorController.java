package group3.topmessage.controller.web;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Handles errors in production environment and provides an error page when
 * an error happens.
 */
@Profile({"production"})
@Controller
public class ProductionErrorController implements ErrorController {

    @RequestMapping("/error")
    public String handleError(Model model, HttpServletRequest request) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        Object message = request.getAttribute(RequestDispatcher.ERROR_MESSAGE);

        model.addAttribute("error", message != null ? message : "");
        model.addAttribute("status", status);

        return "error";
    }

    @Override
    public String getErrorPath() {
        return "error";
    }
}