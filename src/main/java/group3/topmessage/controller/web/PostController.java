package group3.topmessage.controller.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.server.ResponseStatusException;

import group3.topmessage.persistence.entity.Post;
import group3.topmessage.persistence.entity.User;
import group3.topmessage.service.PostService;
import group3.topmessage.service.UserService;

/**
 * Controller for {@link Post} related web pages.
 */
@Controller
public class PostController {

    public static final String BASE_URL = "/posts";

    @Autowired
    private PostService postService;

    @Autowired
    private UserService userService;

    /**
     * Get request mapping for a page containing opening posts.
     * @param model Model for the view template
     * @return template "index"
     */
    @GetMapping(BASE_URL)
    public String viewPosts(Model model) {
        User user = userService.getAuthenticatedUser();
        model.addAttribute("authenticatedUser", user);

        return "index";
    }

    /**
     * Get request mapping for a page of one {@link Post}
     * @param model Model for the view template
     * @return template "post"
     * @throws ResponseStatusException with HTTPStatus.NOT_FOUND
     *   if no Post exists with the id in the path variable
     */
    @GetMapping(BASE_URL+"/{id}")
    public String viewPost(@PathVariable Long id, Model model) {
        User user = userService.getAuthenticatedUser();
        Post post = postService
            .getOnePost(id)
            .orElseThrow(() -> {
                return new ResponseStatusException(HttpStatus.NOT_FOUND);
            });

        model.addAttribute("authenticatedUser", user);
        model.addAttribute("post", post);
        return "post";
    }
}
