package group3.topmessage.controller.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.server.ResponseStatusException;

import group3.topmessage.persistence.entity.User;
import group3.topmessage.service.UserService;

/**
 * Controller for {@link User} related web pages.
 */
@Controller
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * Get request mapping for viewing a user's profile
     * @param model Model for the view
     * @param username username of the user of the profile
     * @return template "profile"
     */
    @GetMapping("/users/{username}")
    public String viewProfile(Model model, @PathVariable String username) {
        User authenticatedUser = userService.getAuthenticatedUser();
        User profileUser = userService.getUserByUsername(username);

        if (profileUser == null) {
            throw new ResponseStatusException(
                HttpStatus.NOT_FOUND,
                "User with username "+username+" does not exist"
            );
        }
        model.addAttribute("authenticatedUser", authenticatedUser);
        model.addAttribute("user", profileUser);

        return "profile";
    }
}
