package group3.topmessage.validation.constraint;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import org.springframework.web.multipart.MultipartFile;

/**
 * Validation constraint used in {@link group3.topmessage.controller.rest.UserRestController} to
 * check that the provided MultipartFile's content type is either
 * image/jpeg or image/png.
 */
@Constraint(validatedBy = ProfilePictureConstraint.Validator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface ProfilePictureConstraint {
    String message() default "Profile picture should be either Jpeg or Png.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

    public class Validator
                implements ConstraintValidator<ProfilePictureConstraint, MultipartFile> {
        @Override
        public void initialize(ProfilePictureConstraint constraintAnnotation) {
        }

        @Override
        public boolean isValid(MultipartFile multipartFile,
                               ConstraintValidatorContext constraintValidatorContext) {
            return multipartFile != null &&
                multipartFile.getContentType().matches("image/(png|jpeg)");
        }
    }
}