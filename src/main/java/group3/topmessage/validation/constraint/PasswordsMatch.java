package group3.topmessage.validation.constraint;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import group3.topmessage.service.dto.UserRegistrationDetails;

/**
 * Validation constraint used in {@link UserRegistrationDetails} to
 * check that provided passwords are equal.
 */
@Constraint(validatedBy = PasswordsMatch.Validator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface PasswordsMatch {
    String message() default "";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

    public class Validator
                implements ConstraintValidator<PasswordsMatch, UserRegistrationDetails> {

        @Override
        public void initialize(PasswordsMatch constraintAnnotation) {
        }

        @Override
        public boolean isValid(UserRegistrationDetails registrationDetails,
                               ConstraintValidatorContext constraintValidatorContext) {
            String pass   = registrationDetails.getPassword();
            String repeat = registrationDetails.getPasswordRepeat();

            return pass.equals(repeat);
        }
    }
}
