package group3.topmessage.validation.constraint;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import org.springframework.beans.factory.annotation.Autowired;

import group3.topmessage.persistence.repository.UserRepository;

/**
 * Validation constraint used in {@link group3.topmessage.service.dto.UserRegistrationDetails} to
 * check that provided username or email has not been taken yet.
 */
@Constraint(validatedBy = NotAlreadyTaken.Validator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface NotAlreadyTaken {

    public enum Field {
        USERNAME, EMAIL
    }

    String message() default "";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    Field field();
    
    public class Validator
                implements ConstraintValidator<NotAlreadyTaken, String> {

        @Autowired
        private UserRepository userRepository;

        private Field field;

        @Override
        public void initialize(NotAlreadyTaken constraintAnnotation) {
            this.field = constraintAnnotation.field();
        }

        @Override
        public boolean isValid(String value,
                               ConstraintValidatorContext constraintValidatorContext) {
            if (field == Field.USERNAME) {
                return userRepository.findByUsername(value) == null;
            }
            return userRepository.findByEmail(value) == null;
        }
    }
}
