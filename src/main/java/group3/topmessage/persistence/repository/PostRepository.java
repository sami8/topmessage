package group3.topmessage.persistence.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import group3.topmessage.persistence.entity.Post;
import group3.topmessage.persistence.entity.User;

/**
 * A repository interface for {@link Post} entities.
 */
public interface PostRepository extends JpaRepository<Post, Long>, PostRepositoryCustom {

    /**
     * Find a {@link Page} of {@link Post}s that don't reply to any other Post.
     * @param pageRequest
     * @return a {@link Page} of {@link Post}s
     */
    Page<Post> findByRepliesToIsNull(Pageable pageRequest);

    /**
     * Find a {@link Page} of {@link Post}s that reply to the Post "repliesTo".
     * @param repliesTo
     * @param pageRequest
     * @return a {@link Page} of {@link Post}s
     */
    Page<Post> findByRepliesTo(Post repliesTo, Pageable pageRequest);

    /**
     * Find a {@link Page} of {@link Post}s that were made by the poster.
     * @param poster
     * @param pageRequest
     * @return a {@link Page} of {@link Post}s
     */
    Page<Post> findByPoster(User poster, Pageable pageRequest);

    /**
     * Find a {@link Page} of {@link Post}s that were made by the poster and reply to the
     * post "repliesTo".
     * @param poster
     * @param pageRequest
     * @return a {@link Page} of {@link Post}s
     */
    Page<Post> findByPosterAndRepliesTo(User poster, Post repliesTo, Pageable pageRequest);
}
