package group3.topmessage.persistence.repository;

import group3.topmessage.persistence.entity.Post;

/**
 * Custom Repository interface that is needed for adding refresh functionality
 * to {@link PostRepository}.<br><br>
 * See also {@link PostRepositoryImpl} and {@link PostRepository}.
 */
public interface PostRepositoryCustom {

    /**
     * Refresh the Post instance from the database.
     * @param post Post to refresh
     */
    void refresh(Post post);
}
