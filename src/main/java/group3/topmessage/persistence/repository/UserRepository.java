package group3.topmessage.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import group3.topmessage.persistence.entity.User;

/**
 * A repository interface for {@link User} entities.
 */
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
    User findByEmail(String email);
}
