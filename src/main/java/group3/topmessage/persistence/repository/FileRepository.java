package group3.topmessage.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import group3.topmessage.persistence.entity.File;

/**
 * A repository interface for {@link File} entities.
 */
public interface FileRepository extends JpaRepository<File, Long> {
}
