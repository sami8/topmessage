package group3.topmessage.persistence.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import group3.topmessage.persistence.entity.Post;

/**
 * Spring automatically integrates this implementation to the interface 
 * that extends {@link PostRepositoryCustom}.
 * Uses {@link EntityManager} to provide refreshing functionality.
 * <br><br>
 * See also {@link PostRepository}.
 */
public class PostRepositoryImpl implements PostRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public void refresh(Post post) {
        entityManager.refresh(post);
    }
}
