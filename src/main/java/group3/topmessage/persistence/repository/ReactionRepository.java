package group3.topmessage.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import group3.topmessage.persistence.entity.Post;
import group3.topmessage.persistence.entity.Reaction;
import group3.topmessage.persistence.entity.User;

/**
 * A repository interface for {@link Reaction} entities.
 */
public interface ReactionRepository extends JpaRepository<Reaction, Long> {
    Reaction findByReactedByAndReactedTo(User reacterBy, Post reactedTo);
    Reaction findByReactedByAndReactedToAndReactionType(User reactedBy,
                                                        Post reactedTo,
                                                        Reaction.ReactionType reactionType);
    Reaction findByReactedByAndReactedToAndReactionTypeNotLike(User reactedBy,
                                                             Post reactedTo,
                                                             Reaction.ReactionType reactionType);
}
