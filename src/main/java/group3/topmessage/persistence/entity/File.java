
package group3.topmessage.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.AbstractPersistable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Entity for files.
 */
@Entity
@Table(name = "files")
@Getter @Setter @ToString
@NoArgsConstructor @AllArgsConstructor
public class File extends AbstractPersistable<Long> {

    private String name;
    private String mediaType;
    private Long size;

    @JsonIgnore @ToString.Exclude
    @Basic(fetch = FetchType.LAZY)
    @Lob @Type(type = "org.hibernate.type.BinaryType")
    private byte[] content;
    
}
