package group3.topmessage.persistence.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.AbstractPersistable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Entity for different kinds of reactions to posts by users.
 */
@Entity
@Table(name = "reactions")
@Getter @Setter @ToString
@NoArgsConstructor @AllArgsConstructor
public class Reaction extends AbstractPersistable<Long> {

    public enum ReactionType {
        LIKE, SMILE, HEART, HEARTS, ANGRY, SAD, LAUGH
    }

    @ManyToOne
    private User reactedBy;

    @ManyToOne
    private Post reactedTo;

    @Enumerated(EnumType.STRING)
    private ReactionType reactionType;
}

