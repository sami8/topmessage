package group3.topmessage.persistence.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.springframework.data.jpa.domain.AbstractPersistable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Entity for users.
 */
@Entity
@Table(name = "users")
@Getter @Setter @ToString
@NoArgsConstructor @AllArgsConstructor
public class User extends AbstractPersistable<Long> {

    @Size(min = 3, max = 14)
    @Column(unique = true)
    private String username;

    private String name;

    @Email
    @Column(unique = true)
    private String email;

    private String sex;
    
    private LocalDate dateOfBirth = LocalDate.of(2000, 1, 1);

    private String description = "";

    private String pictureDescription = "";

    private String city = "";

    private String country = "";

    @JsonIgnore
    private String password;

    @OneToOne
    private File profilePicture;

    @JsonIgnore @ToString.Exclude
    @OneToMany(mappedBy = "reactedBy")
    private List<Reaction> reactions = new ArrayList<>();

    @JsonIgnore @ToString.Exclude
    @OneToMany(mappedBy = "poster")
    private List<Post> posts = new ArrayList<>();
}
