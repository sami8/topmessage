package group3.topmessage.persistence.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.hibernate.annotations.Formula;
import org.springframework.data.jpa.domain.AbstractPersistable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Entity for posts made by users.
 */
@Entity
@Table(name = "posts")
@Getter @Setter @ToString
@NoArgsConstructor @AllArgsConstructor
public class Post extends AbstractPersistable<Long> {

    @ManyToOne
    private User poster;

    @Size(min = 1, max = 500)
    @NotNull
    private String content;

    @JsonIgnore
    @ManyToOne
    private Post repliesTo;

    @JsonIgnore @ToString.Exclude
    @OneToMany(mappedBy = "repliesTo", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Post> replies = new ArrayList<>();

    @JsonIgnore @ToString.Exclude
    @OneToMany(mappedBy = "reactedTo", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Reaction> reactions = new ArrayList<>();

    private LocalDateTime timestamp;

    private LocalDateTime updated;

    @Formula("(select count(*) from reactions r where r.reacted_to_id = id)")
    private Long reactionCount;

    @Formula("(select count(*) from reactions r where r.reacted_to_id = id AND r.reaction_type = 'LIKE')")
    private Long likeCount;

    @Formula("(select count(*) from reactions r where r.reacted_to_id = id AND r.reaction_type = 'SMILE')")
    private Long smileCount;

    @Formula("(select count(*) from reactions r where r.reacted_to_id = id AND r.reaction_type = 'HEART')")
    private Long heartCount;

    @Formula("(select count(*) from reactions r where r.reacted_to_id = id AND r.reaction_type = 'HEARTS')")
    private Long heartsCount;

    @Formula("(select count(*) from reactions r where r.reacted_to_id = id AND r.reaction_type = 'ANGRY')")
    private Long angryCount;

    @Formula("(select count(*) from reactions r where r.reacted_to_id = id AND r.reaction_type = 'SAD')")
    private Long sadCount;

    @Formula("(select count(*) from reactions r where r.reacted_to_id = id AND r.reaction_type = 'LAUGH')")
    private Long laughCount;

    @JsonProperty("replyCount")
    public Integer getReplyCount() {
        return replies.size();
    }

    @JsonProperty("repliesToId")
    public Long getRepliesToId() {
        if (repliesTo == null) {
            return null;
        }
        return repliesTo.getId();
    }

    public void setContent(String content) {
        this.content = content.trim();
    }
}
