package group3.topmessage;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import group3.topmessage.controller.rest.UserRestController;
import group3.topmessage.persistence.entity.File;
import group3.topmessage.persistence.entity.User;
import group3.topmessage.persistence.repository.UserRepository;
import group3.topmessage.service.TestService;
import group3.topmessage.service.UserService;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class UserRestControllerTests {

    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TestService testService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    private MockMvc mockMvc;

    private String baseUrl = UserRestController.BASE_URL;
    
    @Before
    public void reset() throws Exception {
        testService.resetDatabase();
        testService.addTestUser();

        mockMvc = MockMvcBuilders
            .webAppContextSetup(context)
            .apply(springSecurity())
            .build();
    }

    @Test
    @WithMockUser(username = "testuser", password = "password", roles = "user")
    public void userCanBeUpdated() throws Exception {
        User u = userService.getAuthenticatedUser();

        User requestBodyUser = new User();
        requestBodyUser.setDescription("new description");
        requestBodyUser.setName("new name");
        requestBodyUser.setEmail("a@b.com");

        String requestBody = objectMapper.writeValueAsString(requestBodyUser);

        mockMvc.perform(
            put(baseUrl + "/" + u.getId())
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .with(csrf())  
        ).andExpect(status().isOk());

        User after = userRepository.findByUsername(u.getUsername());

        Assert.assertEquals(requestBodyUser.getDescription(), after.getDescription());
        Assert.assertEquals(requestBodyUser.getEmail(), after.getEmail());
        Assert.assertEquals(requestBodyUser.getName(), after.getName());
    }

    @Test
    @WithMockUser(username = "testuser", password = "password", roles = "user")
    public void attemptToUpdateNonExistentUserShouldRespondWithNotFound() throws Exception {
        List<User> usersInDb = userRepository.findAll();
        Long highestId =
            usersInDb.stream()
                .mapToLong(p -> p.getId())
                .max().orElse(1);

        User requestBodyUser = new User();
        requestBodyUser.setDescription("new description");
        requestBodyUser.setName("new name");
        requestBodyUser.setEmail("a@b.com");

        String requestBody = objectMapper.writeValueAsString(requestBodyUser);

        mockMvc.perform(
            put(baseUrl + "/" + (highestId+1))
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .with(csrf())  
        ).andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "testuser", password = "password", roles = "user")
    public void certainFieldsShouldBeIngoredOnUpdate() throws Exception {
        User u = userService.getAuthenticatedUser();

        String usernameBefore = u.getUsername();
        String passwBefore = u.getPassword();

        User requestBodyUser = new User();
        requestBodyUser.setUsername("new username");
        requestBodyUser.setPassword("new passwd");

        String requestBody = objectMapper.writeValueAsString(requestBodyUser);

        mockMvc.perform(
            put(baseUrl + "/" + u.getId())
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .with(csrf())  
        ).andExpect(status().isOk());

        User after = userRepository.findByUsername(u.getUsername());

        Assert.assertNotEquals(requestBodyUser.getUsername(), after.getUsername());
        Assert.assertNotEquals(requestBodyUser.getPassword(), after.getPassword());
        Assert.assertEquals(usernameBefore, after.getUsername());
        Assert.assertEquals(passwBefore, after.getPassword());
    }

    @Test
    @WithMockUser(username = "testuser", password = "password", roles = "user")
    public void cannotUpdateWrongUsersDetails() throws Exception {
        String username = "tmpuser";
        String password = "password";
        String email = "aa@bb.com";
        String hashedPassword = passwordEncoder.encode(password);

        User u = new User();
        u.setUsername(username);
        u.setPassword(hashedPassword);
        u.setEmail(email);

        User savedUser = userRepository.save(u);

        User requestBodyUser = new User();
        requestBodyUser.setDescription("new description");
        requestBodyUser.setName("new name");
        requestBodyUser.setEmail("a@b.com");

        String requestBody = objectMapper.writeValueAsString(requestBodyUser);

        mockMvc.perform(
            put(baseUrl + "/" + savedUser.getId())
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .with(csrf())  
        ).andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = "testuser", password = "password", roles = "user")
    public void usersPasswordShouldNotBeExposed() throws Exception {
        User u = userService.getAuthenticatedUser();

        MvcResult result = mockMvc.perform(get(baseUrl + "/" + u.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andReturn();

        User resultUser =
            objectMapper.readValue(result.getResponse().getContentAsString(), User.class);
        
        Assert.assertTrue(resultUser.getPassword() == null);
    }

    @Test
    @WithMockUser(username = "testuser", password = "password", roles = "user")
    public void profilePictureCanBeUploaded() throws Exception {
        User authenticated = userRepository.findByUsername("testuser");

        String resourceName = "no_image.jpg";
        MockMultipartFile mockMultipartFile =
            mockMultipartFileFromResource(resourceName, "image/jpeg");
        byte[] mockFileBytes = mockMultipartFile.getBytes();

        mockMvc.perform(
            multipart(baseUrl+"/"+authenticated.getId()+"/profilepic")
                .file(mockMultipartFile)
                .with(csrf())
        ).andExpect(status().isOk());

        User u = userRepository.findByUsername("testuser");
        File profilePic = u.getProfilePicture();
        Assert.assertTrue(
            "User's profile picture was not updated.",
            profilePic != null && Arrays.equals(mockFileBytes, profilePic.getContent())
        );
    }

    @Test
    @WithMockUser(username = "testuser", password = "password", roles = "user")
    public void profilePictureHasToBeJpegOrPng() throws Exception {
        User authenticated = userService.getAuthenticatedUser();

        String resourceName = "some_text_file";
        MockMultipartFile mockMultipartFile =
            mockMultipartFileFromResource(resourceName, "text/plain");

        mockMvc.perform(
            multipart(baseUrl+"/"+authenticated.getId()+"/profilepic")
                .file(mockMultipartFile)
                .with(csrf())
        ).andExpect(status().isBadRequest());
    }

    private MockMultipartFile mockMultipartFileFromResource(String resourceName, String contentType) throws IOException {
        String fileResource     = getClass().getResource(resourceName).getFile();
        java.io.File file       = new java.io.File(fileResource);
        String absolutePath     = file.getAbsolutePath();
        InputStream inputStream = Files.newInputStream(Paths.get(absolutePath));

        MockMultipartFile mockMultipartFile =
            new MockMultipartFile("file", resourceName, contentType, inputStream);

        return mockMultipartFile;
    }
}
