package group3.topmessage;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import group3.topmessage.controller.rest.FileRestController;
import group3.topmessage.persistence.entity.File;
import group3.topmessage.persistence.repository.FileRepository;
import group3.topmessage.service.TestService;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class FileRestControllerTests {
    
    @Autowired
    private WebApplicationContext context;

    @Autowired
    private TestService testService;

    @Autowired
    private FileRepository fileRepository;

    private MockMvc mockMvc;

    private String baseUrl = FileRestController.BASE_URL;

    @Before
    public void reset() throws Exception {
        testService.resetDatabase();
        testService.addTestUser();

        mockMvc = MockMvcBuilders
            .webAppContextSetup(context)
            .apply(springSecurity())
            .build();
    }

    @Test
    @WithMockUser(username = "testuser", password = "password", roles = "user")
    public void fileUploadWorks() throws Exception {
        List<File> filesInDBbefore = fileRepository.findAll();

        String resourceName = "no_image.jpg";
        MockMultipartFile mockMultipartFile =
            mockMultipartFileFromResource(resourceName, "image/jpeg");
        byte[] mockFileBytes = mockMultipartFile.getBytes();

        mockMvc.perform(
            multipart(baseUrl)
                .file(mockMultipartFile)
                .with(csrf())
        ).andExpect(status().isOk());

        List<File> filesInDBafter = fileRepository.findAll();

        Assert.assertEquals(filesInDBbefore.size() + 1, filesInDBafter.size());
        Assert.assertTrue(
            filesInDBafter.stream()
                .anyMatch(f -> Arrays.equals(f.getContent(), mockFileBytes))
        );
    }

    private MockMultipartFile mockMultipartFileFromResource(String resourceName, String contentType) throws IOException {
        String fileResource     = getClass().getResource(resourceName).getFile();
        java.io.File file       = new java.io.File(fileResource);
        String absolutePath     = file.getAbsolutePath();
        InputStream inputStream = Files.newInputStream(Paths.get(absolutePath));

        MockMultipartFile mockMultipartFile =
            new MockMultipartFile("file", resourceName, contentType, inputStream);

        return mockMultipartFile;
    }
}
