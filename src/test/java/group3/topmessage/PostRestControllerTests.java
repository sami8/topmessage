package group3.topmessage;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import group3.topmessage.controller.rest.PostRestController;
import group3.topmessage.persistence.entity.Post;
import group3.topmessage.persistence.entity.Reaction;
import group3.topmessage.persistence.entity.Reaction.ReactionType;
import group3.topmessage.persistence.entity.User;
import group3.topmessage.persistence.repository.PostRepository;
import group3.topmessage.persistence.repository.ReactionRepository;
import group3.topmessage.persistence.repository.UserRepository;
import group3.topmessage.service.PostService;
import group3.topmessage.service.TestService;
import group3.topmessage.service.UserService;
import group3.topmessage.service.dto.UserRegistrationDetails;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class PostRestControllerTests {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TestService testService;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private PostService postService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ReactionRepository reactionRepository;

    private MockMvc mockMvc;

    private String baseUrl = PostRestController.BASE_URL;
    
    @Before
    public void reset() throws Exception {
        testService.resetDatabase();
        testService.addTestUser();

        mockMvc = MockMvcBuilders
            .webAppContextSetup(context)
            .apply(springSecurity())
            .build();
    }

    @Test
    @WithMockUser(username = "testuser", password = "password", roles = "user")
    public void getRequestToBaseUrlReturnsAllPostsAsJSON() throws Exception {
        Post p1 = new Post();
        p1.setContent("content");
        postService.createPost(p1);

        Post p2 = new Post();
        p2.setContent("content 2");
        postService.createPost(p2);

        mockMvc.perform(get(baseUrl))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andReturn();
    }

    @Test
    @WithMockUser(username = "testuser", password = "password", roles = "user")
    public void getRequestToBaseUrlWithIdReturnsOnePost() throws Exception {
        Post p1 = new Post();
        p1.setContent("content");
        Post savedPost = postService.createPost(p1);

        MvcResult result = mockMvc.perform(get(baseUrl + "/" + savedPost.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andReturn();

        String json = result.getResponse().getContentAsString();
        Post   post = objectMapper.readValue(json, Post.class);
        Assert.assertTrue(post.getPoster().getUsername().equals("testuser"));
        Assert.assertTrue(post.getContent().equals(p1.getContent()));
    }

    @Test
    @WithMockUser(username = "testuser", password = "password", roles = "user")
    public void getRequestToBaseUrlWithIdRespondsWithNotFoundIfPostDoesNotExist() throws Exception {
        List<Post> postsInDB = postRepository.findAll();
        Long highestId =
            postsInDB.stream()
                .mapToLong(p -> p.getId())
                .max().orElse(1);

        mockMvc.perform(get(baseUrl + "/" + highestId + 1))
            .andExpect(status().is(HttpStatus.NOT_FOUND.value()));
    }

    @Test
    @WithMockUser(username = "testuser", password = "password", roles = "user")
    public void postCanBeDeleted() throws Exception {
        Post p1 = new Post();
        p1.setContent("content");
        Post savedPost = postService.createPost(p1);

        List<Post> postsInDbBefore = postRepository.findAll();
        Assert.assertTrue(postsInDbBefore.contains(savedPost));

        mockMvc.perform(
            delete(baseUrl + "/" + savedPost.getId())
                .with(csrf())  
        ).andExpect(status().isNoContent());

        List<Post> postsInDbAfter = postRepository.findAll();
        Assert.assertTrue(
            "Post still exists in the database after DELETE request.",
            !postsInDbAfter.contains(savedPost));
        Assert.assertTrue(
            "Total amount of Posts after deletion should be one less.",
            postsInDbAfter.size() == postsInDbBefore.size()-1
        );
    }

    @Test
    @WithMockUser(username = "testuser", password = "password", roles = "user")
    public void onlyPosterOfAPostCanDeleteThePost() throws Exception {
        UserRegistrationDetails tmpUser = new UserRegistrationDetails();
        tmpUser.setUsername("username");
        tmpUser.setPassword("password");
        tmpUser.setPasswordRepeat("password");
        userService.register(tmpUser);
        User u = userRepository.findByUsername("username");

        Post post = new Post();
        post.setContent("content");
        post.setPoster(u);
        Post savedPost = postRepository.save(post);

        mockMvc.perform(
            delete(baseUrl + "/" + savedPost.getId())
                .with(csrf())  
        ).andExpect(status().is(HttpStatus.UNAUTHORIZED.value()));
    }

    @Test
    @WithMockUser(username = "testuser", password = "password", roles = "user")
    public void userCanCreateAPost() throws Exception {
        String requestBody = makePostJson("message");

        MvcResult result = mockMvc.perform(
            post(baseUrl)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody)
                .with(csrf())  
        ).andExpect(status().isOk()).andReturn();

        Assert.assertTrue(result.getResponse().getContentAsString().contains("message"));
    }

    @Test
    public void cannotCreateAPostIfNotAuthenticated() throws Exception {
        String requestBody = makePostJson("message");

        mockMvc.perform(
            post(baseUrl)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody)
                .with(csrf())  
        ).andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser(username = "testuser", password = "password", roles = "user")
    public void cannotCreateAPostWithEmptyContent() throws Exception {
        String requestBody = makePostJson("");

        mockMvc.perform(
            post(baseUrl)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody)
                .with(csrf())  
        ).andExpect(status().is(HttpStatus.BAD_REQUEST.value()));

        String onlyWhitespace = makePostJson("    ");

        mockMvc.perform(
            post(baseUrl)
                .contentType(MediaType.APPLICATION_JSON)
                .content(onlyWhitespace)
                .with(csrf())  
        ).andExpect(status().is(HttpStatus.BAD_REQUEST.value()));
    }

    @Test
    @WithMockUser(username = "testuser", password = "password", roles = "user")
    public void canCreateAcommentToAPost() throws Exception {
        String requestBody = makePostJson("message");

        mockMvc.perform(
            post(baseUrl)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody)
                .with(csrf())  
        ).andExpect(status().isOk()).andReturn();

        Assert.assertTrue(
            postRepository.findAll().stream()
                .anyMatch(p -> p.getContent().equals("message"))
        );

        Post postInDB = postRepository.findAll().stream()
            .filter(p -> p.getContent().equals("message"))
            .findFirst().get();

        String requestBodyComment = makePostJson("comment");

        mockMvc.perform(
            post(baseUrl + "/" + postInDB.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBodyComment)
                .with(csrf())  
        ).andExpect(status().isOk());

        Assert.assertTrue(
            postRepository.findAll().stream()
                .anyMatch(p -> p.getContent().equals("comment"))
        );
    }

    @Test
    @WithMockUser(username = "testuser", password = "password", roles = "user")
    public void shouldRespondWithBadRequestIfMissingPostContent() throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("notContent", "zxcgr");
        String json = objectMapper.writeValueAsString(map);

        mockMvc.perform(
            post(baseUrl)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .with(csrf())  
        ).andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "testuser", password = "password", roles = "user")
    public void shouldRespondWithBadRequestIfTryingToReplyToNonExistentPost() throws Exception {
        String json = makePostJson("message");
        List<Post> postsInDB = postRepository.findAll();
        Long highestId =
            postsInDB.stream()
                .mapToLong(p -> p.getId())
                .max().orElse(1);
        
        mockMvc.perform(
            post(baseUrl + "/" + (highestId+1))
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .with(csrf())  
        ).andExpect(status().is(HttpStatus.NOT_FOUND.value()));
    }

    @Test
    @WithMockUser(username = "testuser", password = "password", roles = "user")
    public void postCanBeUpdated() throws Exception {
        String requestBody = makePostJson("message");

        mockMvc.perform(
            post(baseUrl)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody)
                .with(csrf())  
        )
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        Assert.assertTrue(
            "Post doesn't exist in the database after post request to /posts",
            postRepository.findAll().stream()
                .anyMatch(p -> p.getContent().equals("message"))
        );

        Post postInDB = postRepository.findAll().stream()
            .filter(p -> p.getContent().equals("message"))
            .findFirst().get();

        Assert.assertTrue("When post is created, updated should be null.", postInDB.getUpdated() == null);

        String newContent = makePostJson("new content");

        mockMvc.perform(
            put(baseUrl + "/" + postInDB.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(newContent)
                .with(csrf())  
        )
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        Assert.assertTrue(
            "Put request to /posts/{id} did not properly update the post",
            postRepository.findAll().stream()
                .anyMatch(p -> p.getContent().equals("new content"))
        );

        Post updatedPost = postRepository.findAll().stream()
            .filter(p -> p.getContent().equals("new content"))
            .findFirst().get();

        Assert.assertTrue("When post is updated, updated should not be null.", updatedPost.getUpdated() != null);
    }

    @Test
    @WithMockUser(username = "testuser", password = "password", roles = "user")
    public void attemptToUpdateNonExistentPostShouldRespondWithNotFound() throws Exception {
        String newContent = makePostJson("new content");
        List<Post> postsInDB = postRepository.findAll();
        Long highestId =
            postsInDB.stream()
                .mapToLong(p -> p.getId())
                .max().orElse(1);

        mockMvc.perform(
            put(baseUrl+ "/" +(highestId+1))
                .contentType(MediaType.APPLICATION_JSON)
                .content(newContent)
                .with(csrf())  
        ).andExpect(status().is(HttpStatus.NOT_FOUND.value()));
    }

    @Test
    @WithMockUser(username = "testuser", password = "password", roles = "user")
    public void attemptToUpdatePostOfWrongUserShouldRespondWithUnauthorized() throws Exception {
        UserRegistrationDetails tmpUser = new UserRegistrationDetails();
        tmpUser.setUsername("username");
        tmpUser.setPassword("password");
        tmpUser.setPasswordRepeat("password");
        userService.register(tmpUser);

        User u = userRepository.findByUsername("username");

        Post p = new Post();
        p.setContent("content");
        p.setPoster(u);
        p.setTimestamp(LocalDateTime.now());
        Post savedPost = postRepository.save(p);

        String requestBody = makePostJson("message");

        mockMvc.perform(
            put(baseUrl + "/" + savedPost.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody)
                .with(csrf())  
        ).andExpect(status().is(HttpStatus.UNAUTHORIZED.value()));
    }

    @Test
    @WithMockUser(username = "testuser", password = "password", roles = "user")
    public void postCanBeLikedAndExistingCanLikeRemoved() throws Exception {
        User u = userService.getAuthenticatedUser();

        String requestBody = makePostJson("message");

        mockMvc.perform(
            post(baseUrl)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody)
                .with(csrf())  
        ).andExpect(status().isOk());

        Optional<Post> postOpt = postRepository.findAll()
            .stream().filter(p -> p.getContent().equals("message"))
            .findFirst();
        Assert.assertTrue(postOpt.isPresent());
        Post post = postOpt.get();

        Reaction reaction = reactionRepository
            .findByReactedByAndReactedToAndReactionType(u, post, ReactionType.LIKE);

        Assert.assertTrue(
            "Like count for a new post should initially be zero",
            reaction == null
        );

        String requestBody2 = "{ \"reactionType\": \"LIKE\" }";

        mockMvc.perform(
            post(baseUrl + "/" + post.getId() + "/reactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody2)
                .with(csrf())  
        ).andExpect(status().isOk());

        reaction = reactionRepository
            .findByReactedByAndReactedToAndReactionType(u, post, ReactionType.LIKE);

        Assert.assertTrue(
            "Post request failed to add a new like.",
            reaction != null
        );

        mockMvc.perform(
            post(baseUrl + "/" + post.getId() + "/reactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody2)
                .with(csrf())  
        ).andExpect(status().isOk()).andReturn();

        reaction = reactionRepository
            .findByReactedByAndReactedToAndReactionType(u, post, ReactionType.LIKE);

        Assert.assertTrue(
            "Post request failed to remove a like.",
            reaction == null
        );
    }

    private String makePostJson(String content) throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("content", content);
        String json = objectMapper.writeValueAsString(map);

        return json;
    }
}
