package group3.topmessage;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import group3.topmessage.persistence.entity.User;
import group3.topmessage.persistence.repository.UserRepository;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class UserControllerTests {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private UserRepository userRepository;

    private MockMvc mockMvc;

    @Before
    public void reset() throws Exception {
        mockMvc = MockMvcBuilders
            .webAppContextSetup(context)
            .apply(springSecurity())
            .build();
    }
 
    @Test
    public void userCanCreateAnAccount() throws Exception {
        String username = "user";
        String passwd = "password123";
        String email = "a@b.com";
        List<User> usersInDbBefore = userRepository.findAll();

        mockMvc.perform(
            post("/signin")
                .param("username", username)
                .param("password", passwd)
                .param("passwordRepeat", passwd)
                .param("email", email)
                .with(csrf())
        ).andExpect(status().is3xxRedirection());

        List<User> usersInDbAfter = userRepository.findAll();

        Assert.assertEquals(usersInDbAfter.size(), usersInDbBefore.size() + 1);
        Assert.assertTrue(usersInDbAfter.stream().anyMatch(u -> u.getUsername().equals(username)));
    }
}
