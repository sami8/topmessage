package group3.topmessage;

import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import group3.topmessage.service.LocaleFileService;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class LocaleFileServiceTests {

    @Autowired
    private LocaleFileService localeFileService;

    @Test
    public void serviceFindsExpectedFiles() throws Exception {
        Map<String, String> result = localeFileService.availableVueLocaleFiles();

        Assert.assertTrue(
            "Finnish translations were not found", 
            result.containsKey("fi") && result.get("fi").equals("vue_messages_fi.json")
        );
        Assert.assertTrue(
            "Default translations were not found", 
            result.containsKey("default") && result.get("default").equals("vue_messages_default.json")
        );
    }
}
