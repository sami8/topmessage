package group3.topmessage;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import group3.topmessage.controller.web.PostController;
import group3.topmessage.service.TestService;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class PostControllerTests {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private TestService testService;

    private MockMvc mockMvc;

    private String baseUrl = PostController.BASE_URL;

    @Before
    public void reset() throws Exception {
        testService.resetDatabase();
        testService.addTestUser();

        mockMvc = MockMvcBuilders
            .webAppContextSetup(context)
            .apply(springSecurity())
            .build();
    }

    @Test
    @WithMockUser(username = "testuser", password = "password", roles = "user")
    public void atBaseUrlCorrectViewIsReturned() throws Exception {
        MvcResult result = mockMvc.perform(
            get(baseUrl)
        ).andExpect(status().isOk()).andReturn();

        ModelAndView mw = result.getModelAndView();
        Assert.assertTrue("Incorrect view was returned.", mw.getViewName().equals("index"));
    }
}
