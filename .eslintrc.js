module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'plugin:vue/base',
    'eslint:recommended'
  ],
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module',
  },
  rules: {
    'vue/html-indent': ['error', 2, {}],
    'no-undef': 0,
    eqeqeq: 'error',
    'no-var': 'error',
    'prefer-const': [
      'error',
      {
        destructuring: 'any',
        ignoreReadBeforeAssign: false,
      },
    ],
    'no-await-in-loop': 'error',

    indent: [2, 2, { SwitchCase: 1 }],
    quotes: ['error', 'single'],
    semi: ['error', 'never'],
    'no-unused-vars': [
      'warn',
      {
        argsIgnorePattern: '^_',
      },
    ],
    'no-trailing-spaces': 'error',
    'object-curly-spacing': ['error', 'always'],
    'arrow-spacing': ['error', { before: true, after: true }],
    'arrow-parens': ['error', 'always'],
    'brace-style': 'error',
    camelcase: 'error',
    'comma-style': ['error', 'last'],
    'semi-spacing': 'error',
    'switch-colon-spacing': ['error', { after: true, before: false }],
    'array-callback-return': ['error', { allowImplicit: true }],
    'comma-spacing': ['error', { before: false, after: true }],
    curly: 'error',
    'dot-notation': 'error',
    'key-spacing': ['error', { afterColon: true, beforeColon: false }],
    'space-before-blocks': 'error',
    'keyword-spacing': ['error', { before: true, after: true }],
    'space-before-function-paren': [
      'error',
      { anonymous: 'always', named: 'never', asyncArrow: 'always' },
    ],
    'no-constant-condition': 'error',
    'no-extra-semi': 'error',
    'no-useless-return': 'error',
    'require-await': 'error',
    'no-confusing-arrow': 'error',
    'object-shorthand': ['error', 'always'],
  },
}
