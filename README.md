# topMessage

Metropolian 3. vuoden projektiharjoitustyö.

## Kehitysympäristön asettaminen

Projektin käynnistäminen ja kehittäminen vaatii Maven-koontityökalun ja PostgreSQL-tietokannanhallintajärjestelmän. Javascript-kehitystä varten tarvitsee myös Node.js-Javascript -suoritusympäristön ja NPM-paketinhallintaohjelman.

### Maven
Linux-järjestelmillä Maven löytyy todennäköisesti jakelun omasta paketinhallinnasta.
Mavenin asennusohjeita Windowsille löytyy [täältä](https://maven.apache.org/install.html), ja lataukset [täältä](https://maven.apache.org/download.cgi).

### PostgreSQL

Projektia varten tulee asentaa ja käynnistää PostgreSQL. Kun PostgreSQL:n on asentanut, sen mukana tulee psql-komentokehote.

Avaa psql postgres-käyttäjänä. Linuxilla komento `psql -d postgres` tai Windowilla avaa psql-komentokehotteen
ja syöttää siihen tunnusta kysyttäessä `postgres`.

Uuden käyttäjän voi luoda seuraavasti:
```SQL
CREATE ROLE käyttäjänimi WITH LOGIN PASSWORD 'salasana';
```

Anna käyttäjälle oikeus luoda tietokantoja:
```SQL
ALTER ROLE käyttäjänimi CREATEDB;
```

Sen jälkeen poistu psql:stä `\q`-komennolla ja avaa se uudella käyttäjällä:
```sh
psql -d postgres -U käyttäjätunnus
```
Windowsilla voi taas avata psql-kehotteen ja syöttää tunnuksen kun sitä kysytään.

Luo tietokanta:
```SQL
CREATE DATABASE tietokannan_nimi;
```

Tietokantaan voi yhdistää seuraavasti:
```SQL
\c tietokannan_nimi
```

### Spring profiilit

Projektin polussa [src/main/resources](./src/main/resources) löytyy properties-päätteisiä tiedostoja, joista yksi päättyy "-example".
Nämä ovat Springin profiileja määrittäviä tiedostoja, jotka määrittävät erilaisia asetuksia riippuen esimerkiksi
onko tarkoitus kehittää sovellusta vai ajaa sitä tuotantotilassa.
Kehitystilaa varten polussa on esimerkkitiedosto [application-dev.properties-example](./src/main/resources/application-dev.properties-example).
Tiedosto tulee tehdä kopio nimellä "application-dev.properties" ja siihen tulee asettaa PostgreSQL:n osoite ja ylempänä tehdyt käyttäjätiedot.
PostgreSQL:n oletusportti on 5432.

### Käynnistäminen

Projektin voi nyt käynnistää komentoriviltä seuraavalla komennolla:
```sh
mvn clean spring-boot:run -Dspring-boot.run.profiles=dev
```
Spring käynnistyy tuolloin "dev"-profiililla.

Testit voi ajaa seuraavasti:
```sh
mvn clean test -Dspring.profiles.active=dev
```

### ESLint

Javascriptia varten projektissa käytetään lintterinä ESLint-työkalua.
Työkalun asetusta varten täytyy ensin asentaa [Node.js](https://nodejs.org/en/) ja NPM-paketinhallintaohjelma. NPM:n sijasta voi vaihtoehtoisesti asentaa Yarn-nimisen ohjelman.
Kun NPM on asennettu, projektin juuressa voi asentaa riippuvuudet ajamalla komentoriviltä:
```sh
npm install
```
Tai Yarnilla:
```sh
yarn install
```

ESLintin voi käynnistää komentoriviltä seuraavasti:
```sh
npx eslint .
```

